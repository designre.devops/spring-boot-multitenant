[![Java Version](https://img.shields.io/badge/Java-19-green.svg)](https://www.oracle.com/java/technologies/javase/20/)
[![Keycloak Version](https://img.shields.io/badge/Keycloak-22-blue.svg)](https://www.keycloak.org/)
[![Spring Boot Version](https://img.shields.io/badge/Spring%20Boot-3.1-brightgreen)](https://spring.io/projects/spring-boot)
[![Spring Boot Cloud Version](https://img.shields.io/badge/Spring%20Boot%20Cloud-2022.0.4-brightgreen)](https://spring.io/projects/spring-boot)
[![Docker Version](https://img.shields.io/badge/Docker-24.0.5-blue)](https://www.docker.com/)
[![Docker Compose Version](https://img.shields.io/badge/Docker%20Compose-v2.20.2-blue)](https://docs.docker.com/compose/)

# Spring Boot Multi-Tenant

---

> Text ...

## Run

- Map *hostname* (tenants) to *ip* address (localhost) <br>
    - For **Linux** run command below
    ```
    sudo vim /etc/hosts
    ```
    - Add an entries at the end of the file to map the domains (tenants) to your *localhost's* IP *address*
    ```
    127.0.0.1   beans.school.com
    127.0.0.1   dukes.school.com
    ```
- Open project in *intellij* and run in it, or use *gradle* to run the project like cli command below,
  this command will start docker compose and run the application

```
./gradlew :apps:gateway:bootRun --args='--spring.docker.compose.file=/absolut-path-to-cloned-project/spring-boot-multitenant/compose.yaml'  
```

## Prerequisites

To run this example locally you need.

- Docker (22.0.0)
- Docker Compose (v2.20.2)
- Java 19

## Links

- http://beans.school.com:8080/api/app1/push
