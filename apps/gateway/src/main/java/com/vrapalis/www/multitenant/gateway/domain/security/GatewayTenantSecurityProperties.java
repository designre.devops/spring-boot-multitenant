package com.vrapalis.www.multitenant.gateway.domain.security;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URI;

@ConfigurationProperties(prefix = "tenant.security")
public record GatewayTenantSecurityProperties(
        URI issuerBaseUri,
        String clientId,
        String clientSecret) {
}
