package com.vrapalis.www.multitenant.tenant.domain.tenants.car.service;

import com.vrapalis.www.multitenant.tenant.domain.tenants.car.dto.CarDto;

public interface ICarService {
    CarDto create(CarDto carDto);
}
