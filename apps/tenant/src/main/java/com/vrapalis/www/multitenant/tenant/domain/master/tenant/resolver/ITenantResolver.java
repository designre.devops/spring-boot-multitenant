package com.vrapalis.www.multitenant.tenant.domain.master.tenant.resolver;

import jakarta.servlet.http.HttpServletRequest;

import java.util.Optional;

@FunctionalInterface
public interface ITenantResolver {

    Optional<String> resolveTenantName(HttpServletRequest request);
}
