package com.vrapalis.www.multitenant.tenant.domain.tenants.car.service;

import com.vrapalis.www.multitenant.tenant.domain.tenants.car.dto.CarDto;
import com.vrapalis.www.multitenant.tenant.domain.tenants.car.entity.CarEntity;
import com.vrapalis.www.multitenant.tenant.domain.tenants.car.repository.CarRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
public class ICarServiceImpl implements ICarService {

    private final CarRepository carRepository;

    @Override
    public CarDto create(CarDto carDto) {
        val carEntity = carRepository.save(CarEntity.builder().id(carDto.getId()).name(carDto.getName()).build());
        return new CarDto(carEntity.getId(), carEntity.getName());
    }
}
