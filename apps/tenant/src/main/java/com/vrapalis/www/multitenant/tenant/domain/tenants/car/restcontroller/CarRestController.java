package com.vrapalis.www.multitenant.tenant.domain.tenants.car.restcontroller;

import com.vrapalis.www.multitenant.tenant.config.db.TenantIdentifierResolverConfiguration;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.vrapalis.www.multitenant.tenant.domain.tenants.car.dto.CarDto;
import com.vrapalis.www.multitenant.tenant.domain.tenants.car.service.ICarService;

@RestController
@RequestMapping("/api/cars")
@RequiredArgsConstructor
public class CarRestController {

    private final TenantIdentifierResolverConfiguration currentTenant;
    private final ICarService carService;

    @PutMapping
    public void setSchema(@RequestParam String schema) {
        currentTenant.setCurrentTenant(schema);
    }

    @PostMapping
    public ResponseEntity<String> createMercedes(@RequestBody CarDto carDto) {
        carService.create(carDto);
        return ResponseEntity.ok("!!!");
    }

    @GetMapping
    @SneakyThrows
    public ResponseEntity<String> greeting() {
        Thread.sleep(3000);
        return ResponseEntity.ok("Greeting!!!");
    }
}
