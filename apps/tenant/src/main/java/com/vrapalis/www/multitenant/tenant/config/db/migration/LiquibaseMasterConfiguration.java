package com.vrapalis.www.multitenant.tenant.config.db.migration;

import liquibase.integration.spring.SpringLiquibase;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(LiquibaseProperties.class)
@ConditionalOnProperty(name = "multi-tenancy.master.liquibase.enabled", havingValue = "true", matchIfMissing = true)
public class LiquibaseMasterConfiguration {

    @Bean("masterLiquibaseProperties")
    @ConfigurationProperties("multi-tenancy.master.liquibase")
    public LiquibaseProperties masterLiquibaseProperties() {
        return new LiquibaseProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("spring.liquibase")
    public LiquibaseProperties springLiquibase() {
        return new LiquibaseProperties();
    }

    @Bean("tenantsLiquibaseProperties")
    @ConfigurationProperties("multi-tenancy.tenants.liquibase")
    public LiquibaseProperties tenantsLiquibaseProperties() {
        return new LiquibaseProperties();
    }

    @Bean
    public SpringLiquibase liquibase(ObjectProvider<DataSource> liquibaseDataSource) {
        LiquibaseProperties liquibaseProperties = masterLiquibaseProperties();
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(liquibaseDataSource.getIfAvailable());
        liquibase.setChangeLog(liquibaseProperties.getChangeLog());
        liquibase.setContexts(liquibaseProperties.getContexts());
        return liquibase;
    }
}
