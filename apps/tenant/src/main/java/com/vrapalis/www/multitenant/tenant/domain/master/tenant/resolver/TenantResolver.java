package com.vrapalis.www.multitenant.tenant.domain.master.tenant.resolver;

import com.vrapalis.www.multitenant.tenant.domain.master.tenant.property.TenantProperty;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TenantResolver implements ITenantResolver {

    private final TenantProperty tenantProperty;

    @Override
    public Optional<String> resolveTenantName(HttpServletRequest request) {
        val tenantName = request.getHeader(tenantProperty.getTenantHeader());
        log.debug("Tenant resolved to: {}", tenantName);
        return Optional.ofNullable(tenantName);
    }
}
