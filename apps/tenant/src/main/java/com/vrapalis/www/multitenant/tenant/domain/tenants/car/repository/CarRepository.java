package com.vrapalis.www.multitenant.tenant.domain.tenants.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.vrapalis.www.multitenant.tenant.domain.tenants.car.entity.CarEntity;

import java.util.UUID;

@Repository
public interface CarRepository extends JpaRepository<CarEntity, UUID> {
}
