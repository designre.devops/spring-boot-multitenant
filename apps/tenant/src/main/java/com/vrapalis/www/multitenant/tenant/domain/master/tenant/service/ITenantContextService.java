package com.vrapalis.www.multitenant.tenant.domain.master.tenant.service;

public interface ITenantContextService {
    final ThreadLocal<String> TENANT_NAME = new InheritableThreadLocal<>();

    boolean setTenant(String tenantName);
    boolean clearTenant();

    String getCurrentTenant();
}
