package com.vrapalis.www.multitenant.tenant.config.db;

import com.vrapalis.www.multitenant.tenant.domain.master.tenant.service.ITenantContextService;
import lombok.RequiredArgsConstructor;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class TenantIdentifierResolverConfiguration implements CurrentTenantIdentifierResolver, HibernatePropertiesCustomizer {

    public static final String TENANT_DEFAULT = "PUBLIC";
    private final ITenantContextService tenantContextService;


    public void setCurrentTenant(String tenant) {
        tenantContextService.setTenant(Objects.requireNonNull(tenant));
    }

    @Override
    public String resolveCurrentTenantIdentifier() {
        return Objects.requireNonNullElse(tenantContextService.getCurrentTenant(), TENANT_DEFAULT);
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }

    @Override
    public void customize(Map<String, Object> hibernateProperties) {
        hibernateProperties.put(AvailableSettings.MULTI_TENANT_IDENTIFIER_RESOLVER, this);
    }
}
