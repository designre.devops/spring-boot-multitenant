package com.vrapalis.www.multitenant.tenant.domain.master.tenant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.vrapalis.www.multitenant.tenant.domain.master.tenant.entity.TenantEntity;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TenantRepository extends JpaRepository<TenantEntity, UUID> {

    Optional<TenantEntity> findBySchemaName(String schemaName);
}
