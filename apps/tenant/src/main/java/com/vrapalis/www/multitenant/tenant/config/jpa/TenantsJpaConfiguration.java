package com.vrapalis.www.multitenant.tenant.config.jpa;

import jakarta.persistence.EntityManagerFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.SpringBeanContainer;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJpaRepositories(
        basePackages = {"${multi-tenancy.tenants.repositories.packages}"},
        entityManagerFactoryRef = "tenantsEntityManagerFactory",
        transactionManagerRef = "tenantsTransactionManager"
)
@EnableConfigurationProperties(JpaProperties.class)
public class TenantsJpaConfiguration {

    private final ConfigurableListableBeanFactory beanFactory;
    private final JpaProperties jpaProperties;

    public TenantsJpaConfiguration(ConfigurableListableBeanFactory beanFactory, JpaProperties jpaProperties) {
        this.beanFactory = beanFactory;
        this.jpaProperties = jpaProperties;
    }

    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean tenantsEntityManagerFactory(CurrentTenantIdentifierResolver tenantIdentifierResolver,
                                                                              MultiTenantConnectionProvider tenantConnectionProvider) {
        LocalContainerEntityManagerFactoryBean emfBean = new LocalContainerEntityManagerFactoryBean();
        emfBean.setPersistenceUnitName("tenants-persistence-unit");
        emfBean.setPackagesToScan(tenantsJpaProperties().getEntities().getPackages());

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        emfBean.setJpaVendorAdapter(vendorAdapter);

        Map<String, Object> properties = new HashMap<>(this.jpaProperties.getProperties());
//        properties.put(AvailableSettings.PHYSICAL_NAMING_STRATEGY, "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        properties.put(AvailableSettings.IMPLICIT_NAMING_STRATEGY, "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
        properties.put(AvailableSettings.BEAN_CONTAINER, new SpringBeanContainer(this.beanFactory));
        properties.remove(AvailableSettings.DEFAULT_SCHEMA);
//        properties.put(AvailableSettings.MULTI_TENANT, MultiTenancyStrategy.SCHEMA);
        properties.put(AvailableSettings.MULTI_TENANT_CONNECTION_PROVIDER, tenantConnectionProvider);
        properties.put(AvailableSettings.MULTI_TENANT_IDENTIFIER_RESOLVER, tenantIdentifierResolver);
        emfBean.setJpaPropertyMap(properties);

        return emfBean;
    }

    @Bean
    @Primary
    public JpaTransactionManager tenantsTransactionManager(@Qualifier("tenantsEntityManagerFactory") EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean("tenantsJpaProperties")
    @ConfigurationProperties("multi-tenancy.tenants")
    public JpaCustomProperties tenantsJpaProperties() {
        return new JpaCustomProperties();
    }
}
