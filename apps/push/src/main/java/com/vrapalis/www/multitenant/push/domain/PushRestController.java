package com.vrapalis.www.multitenant.push.domain;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/push")
public class PushRestController {

    private final PushService pushService;

    @GetMapping
    public ResponseEntity<String> email(Authentication authentication) {
        JwtAuthenticationToken jwtAuthentication = (JwtAuthenticationToken) authentication;
        log.info("Authentication: {}" + jwtAuthentication.getTokenAttributes().toString());
        return ResponseEntity.ok(pushService.sendEmail());
    }
}
