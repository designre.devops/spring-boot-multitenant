package com.vrapalis.www.multitenant.push.domain;

import lombok.SneakyThrows;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PushServiceImpl implements PushService {

    @Override
    @Cacheable("sendEmail")
    @SneakyThrows
    public String sendEmail() {
        Thread.sleep(5000);
        return "Email send!!!";
    }
}
