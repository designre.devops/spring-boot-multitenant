package com.vrapalis.www.multitenant.multitenancy.domain.tenant;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationManagerResolver;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@RequiredArgsConstructor
public class MultitenancyAuthManagerResolver implements AuthenticationManagerResolver<HttpServletRequest> {

    private static final Map<String,AuthenticationManager> AUTHENTICATION_MANAGERS = new ConcurrentHashMap<>();
    private final MultitenancyTenantHttpProperties tenantHttpProperties;

    @Override
    public AuthenticationManager resolve(HttpServletRequest context) {
        var tenantId = MultitenancyTenantContext.getTenantId();
		return AUTHENTICATION_MANAGERS.computeIfAbsent(tenantId, this::buildAuthenticationManager);
    }

    private AuthenticationManager buildAuthenticationManager(String tenantId) {
		var issuerBaseUri = tenantHttpProperties.issuerBaseUri().toString().strip();
		var issuerUri = issuerBaseUri + tenantId;
		var jwtAuthenticationprovider = new JwtAuthenticationProvider(JwtDecoders.fromIssuerLocation(issuerUri));
		return jwtAuthenticationprovider::authenticate;
	}
}
