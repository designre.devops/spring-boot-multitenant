package com.vrapalis.www.multitenant.multitenancy.domain.tenant;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MultitenancyTenantFilter extends OncePerRequestFilter {

    private final MultitenancyTenantHttpResolverImpl tenantHttpResolver;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Optional.ofNullable(tenantHttpResolver.resolveTenantId(request)).ifPresent(tenantId -> {
            MultitenancyTenantContext.setTenantId(tenantId);
        });
        filterChain.doFilter(request, response);
        MultitenancyTenantContext.clear();
    }
}
