package com.vrapalis.www.multitenant.multitenancy.domain.tenant;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MultitenancyTenantHttpResolverImpl<T extends HttpServletRequest> implements MultitenancyTenantHttpResolver<HttpServletRequest> {

    private final MultitenancyTenantHttpProperties tenantHttpProperties;

    @Override
    public String resolveTenantId(@NonNull HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader(tenantHttpProperties.headerName());
    }
}
