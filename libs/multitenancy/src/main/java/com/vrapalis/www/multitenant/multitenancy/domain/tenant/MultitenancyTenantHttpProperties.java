package com.vrapalis.www.multitenant.multitenancy.domain.tenant;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "multi-tenancy.http")
public record MultitenancyTenantHttpProperties(String headerName, String issuerBaseUri) {
}
