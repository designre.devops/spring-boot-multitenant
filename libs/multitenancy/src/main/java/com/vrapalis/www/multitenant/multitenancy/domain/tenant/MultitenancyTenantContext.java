package com.vrapalis.www.multitenant.multitenancy.domain.tenant;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class MultitenancyTenantContext {

    private static final ThreadLocal<String> TENANT_ID = new InheritableThreadLocal<>();

	public static void setTenantId(String tenant) {
    	log.debug("Setting current tenant to: {}", tenant);
    	TENANT_ID.set(tenant);
	}

	public static String getTenantId() {
    	return TENANT_ID.get();
  	}

	public static void clear() {
    	TENANT_ID.remove();
  	}
}
